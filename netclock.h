#include <memory>
#include <string>
#include <vector>
#include <endian.h>
#include <unordered_map>
#include <chrono>

#ifndef LIBNETCLOCK_NETCLOCK_H
#define LIBNETCLOCK_NETCLOCK_H

namespace NTP {
    //    _SYSTEM_EPOCH = datetime.date(*time.gmtime(0)
    //    [0:3])
    //    """system epoch"""
    //    _NTP_EPOCH = datetime.date(1900, 1, 1)
    //    """NTP epoch"""
    //    NTP_DELTA = (_SYSTEM_EPOCH - _NTP_EPOCH).days * 24 * 3600
    //    """delta between system and NTP time"""

    std::unordered_map <std::string, std::string> REF_ID_TABLE = {
            {"GOES", "Geostationary Orbit Environment Satellite"},
            {"GPS",  "Global Position System"},
            {"GAL",  "Galileo Positioning System"},
            {"PPS",  "Generic pulse-per-second"},
            {"IRIG", "Inter-Range Instrumentation Group"},
            {"WWVB", "LF Radio WWVB Ft. Collins, CO 60 kHz"},
            {"DCF",  "LF Radio DCF77 Mainflingen, DE 77.5 kHz"},
            {"HBG",  "LF Radio HBG Prangins, HB 75 kHz"},
            {"MSF",  "LF Radio MSF Anthorn, UK 60 kHz"},
            {"JJY",  "LF Radio JJY Fukushima, JP 40 kHz, Saga, JP 60 kHz"},
            {"LORC", "MF Radio LORAN C station, 100 kHz"},
            {"TDF",  "MF Radio Allouis, FR 162 kHz"},
            {"CHU",  "HF Radio CHU Ottawa, Ontario"},
            {"WWV",  "HF Radio WWV Ft. Collins, CO"},
            {"WWVH", "HF Radio WWVH Kauai, HI"},
            {"NIST", "NIST telephone modem"},
            {"ACTS", "NIST telephone modem"},
            {"USNO", "USNO telephone modem"},
            {"PTB",  "European telephone modem"},
            {"LOCL", "uncalibrated local clock"},
            {"CESM", "calibrated Cesium clock"},
            {"RBDM", "calibrated Rubidium clock"},
            {"OMEG", "OMEGA radionavigation system"},
            {"DCN",  "DCN routing protocol"},
            {"TSP",  "TSP time protocol"},
            {"DTS",  "Digital Time Service"},
            {"ATOM", "Atomic clock (calibrated)"},
            {"VLF",  "VLF radio (OMEGA,, etc.)"},
            {"1PPS", "External 1 PPS input"},
            {"FREE", "(Internal clock)"},
            {"INIT", "(Initialization)"},
            {"ROA",  "Real Observatorio de la Armada"},
            {"",     "NULL"},
    };

    std::unordered_map<int, std::string> STRATUM_TABLE = {
            {0, "unspecified or invalid (%s)"},
            {1, "primary reference (%s)"},
    };

    std::unordered_map<int, std::string> MODE_TABLE = {
            {0, "reserved"},
            {1, "symmetric active"},
            {2, "symmetric passive"},
            {3, "client"},
            {4, "server"},
            {5, "broadcast"},
            {6, "reserved for NTP control messages"},
            {7, "reserved for private use"},
    };

    std::unordered_map<int, std::string> LEAP_TABLE = {
            {0, "no warning"},
            {1, "last minute of the day has 61 seconds"},
            {2, "last minute of the day has 59 seconds"},
            {3, "unknown (clock unsynchronized)"},
    };

    struct Packet {
        /*
         0                   1                   2                   3
         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |LI | VN  |Mode |    Stratum    |      Poll     |   Precision   |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                      Root Delay (signed)                      |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                        Root Dispersion                        |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                  Reference Clock Identifier                   |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                                                               |
        |                 Reference Timestamp (64 bits)                 |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                                                               |
        |                 Originate Timestamp (64 bits)                 |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                                                               |
        |                  Receive Timestamp (64 bits)                  |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                                                               |
        |                  Transmit Timestamp (64 bits)                 |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         */
        uint8_t leap_indicator;
        uint8_t version_number;
        uint8_t mode;
        uint8_t stratum;
        uint8_t poll;
        int8_t precision;
        double root_delay;
        double root_dispersion;
        char reference_clock_identifier[5];
        std::chrono::nanoseconds reference_timestamp;
        std::chrono::nanoseconds originate_timestamp;
        std::chrono::nanoseconds receive_timestamp;
        std::chrono::nanoseconds transmit_timestamp;
        std::chrono::nanoseconds destination_timestamp;
        static Packet deserialize(std::vector<uint8_t> &aData);
        std::vector<uint8_t> serialize();
    };

    struct NTPStats {
        NTPStats(Packet &aPacket): mPacket(aPacket) {};
        Packet mPacket;
        double offset();
        std::chrono::nanoseconds offset_nanoseconds();
        double delay();
        std::chrono::nanoseconds delay_nanoseconds();
        double tx_time();
        std::chrono::nanoseconds tx_time_nanoseconds();
        double recv_time();
        std::chrono::nanoseconds recv_time_nanoseconds();
        double orig_time();
        std::chrono::nanoseconds orig_time_nanoseconds();
        double ref_time();
        std::chrono::nanoseconds ref_time_nanoseconds();
        double dest_time();
        std::chrono::nanoseconds dest_time_nanoseconds();
    };

    class NTPClient {
    public:
        NTPClient() = default;
        NTPStats request(std::string host, uint16_t port);
    };

    double double_from_unsigned_fixed32(uint32_t fixed);
    uint32_t unsigned_fixed32_from_double(double input);
    double double_from_signed_fixed32(int32_t fixed);
    int32_t signed_fixed32_from_double(double input);

    std::chrono::nanoseconds nano_since1970_from_ntp_timestamp(uint64_t input);
    uint64_t ntp_timestamp_from_nano_since1970(std::chrono::nanoseconds input);
}

#endif //LIBNETCLOCK_NETCLOCK_H
