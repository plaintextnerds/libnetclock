//
// Created by tim on 17/02/2022.
//
#include <iostream>
#include "netclock.h"

int main() {
    NTP::NTPClient client = NTP::NTPClient();
    auto stats = client.request("time.windows.com", 123);
    auto now_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());
    std::cout << "System time (nanoseconds since Unix time epoch):   " << now_ns.count() <<std::endl;
    std::cout << "NTP adjustment (nanoseconds):                      " << stats.offset_nanoseconds().count() << std::endl;
    std::cout << "Adjusted time (nanoseconds since Unix time epoch): " << (now_ns+stats.offset_nanoseconds()).count() <<std::endl;
}

