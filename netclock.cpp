#include "netclock.h"

#include <boost/asio.hpp>
#include <iostream>
#include <cstring>
#include <chrono>

using boost::asio::ip::address;
using boost::asio::ip::make_address;
using boost::asio::buffer;
using boost::asio::io_context;
using boost::asio::ip::udp;
using namespace std::chrono;

namespace NTP {
    Packet Packet::deserialize(std::vector<uint8_t> &aData) {
        Packet packet;
        size_t i = aData.size();
        i -= sizeof(uint64_t);
        uint64_t net_transmit;
        std::memcpy(&net_transmit, aData.data() + i, sizeof(uint64_t));
        uint64_t transmit_int = be64toh(net_transmit);
        packet.transmit_timestamp = nano_since1970_from_ntp_timestamp(transmit_int);
        aData.resize(i);
        i -= sizeof(uint64_t);
        uint64_t net_receive;
        std::memcpy(&net_receive, aData.data() + i, sizeof(uint64_t));
        uint64_t receive_int = be64toh(net_receive);
        packet.receive_timestamp = nano_since1970_from_ntp_timestamp(receive_int);
        aData.resize(i);
        i -= sizeof(uint64_t);
        uint64_t net_originate;
        std::memcpy(&net_originate, aData.data() + i, sizeof(uint64_t));
        uint64_t originate_int = be64toh(net_originate);
        packet.originate_timestamp = nano_since1970_from_ntp_timestamp(originate_int);
        aData.resize(i);
        i -= sizeof(uint64_t);
        uint64_t net_reference;
        std::memcpy(&net_reference, aData.data() + i, sizeof(uint64_t));
        uint64_t reference_int = be64toh(net_reference);
        packet.reference_timestamp = nano_since1970_from_ntp_timestamp(reference_int);
        aData.resize(i);
        i -= 4;  // copy 4 bytes
        std::memcpy(packet.reference_clock_identifier, aData.data() + i, 4);
        aData.resize(i);
        i -= sizeof(uint32_t);
        uint32_t net_root_dispersion;
        std::memcpy(&net_root_dispersion, aData.data() + i, sizeof(uint32_t));
        uint32_t drift = be32toh(net_root_dispersion);
        packet.root_dispersion = double_from_unsigned_fixed32(drift);
        aData.resize(i);
        i -= sizeof(int32_t);
        int32_t net_root_delay;
        std::memcpy(&net_root_delay, aData.data() + i, sizeof(int32_t));
        int32_t error = be32toh(net_root_delay);
        packet.root_delay = double_from_signed_fixed32(error);
        aData.resize(i);
        i -= sizeof(int8_t);
        std::memcpy(&packet.precision, aData.data() + i, sizeof(int8_t));
        aData.resize(i);
        i -= sizeof(uint8_t);
        std::memcpy(&packet.poll, aData.data() + i, sizeof(uint8_t));
        aData.resize(i);
        i -= sizeof(uint8_t);
        std::memcpy(&packet.stratum, aData.data() + i, sizeof(uint8_t));
        aData.resize(i);
        i -= sizeof(uint8_t);
        uint8_t first8;
        std::memcpy(&first8, aData.data() + i, sizeof(uint8_t));
        packet.leap_indicator = first8 >> 6;
        packet.version_number = (first8 >> 3) - (packet.leap_indicator << 3);
        packet.mode = first8 - ((packet.leap_indicator << 6) + (packet.version_number << 3));
        aData.resize(i);
        return packet;
    }

    std::vector<uint8_t> Packet::serialize() {
        std::vector<uint8_t> body;
        size_t i = body.size();
        body.resize(i + sizeof(uint8_t));
        uint8_t first8 = (leap_indicator << 6) + (version_number << 3) + mode;
        std::memcpy(body.data() + i, &first8, sizeof(uint8_t));
        i = body.size();
        body.resize(i + sizeof(stratum));
        std::memcpy(body.data() + i, &stratum, sizeof(stratum));
        i = body.size();
        body.resize(i + sizeof(poll));
        std::memcpy(body.data() + i, &poll, sizeof(poll));
        i = body.size();
        body.resize(i + sizeof(precision));
        std::memcpy(body.data() + i, &precision, sizeof(precision));
        i = body.size();
        body.resize(i + sizeof(int32_t));
        int32_t net_root_delay = htobe32(signed_fixed32_from_double(root_delay));
        std::memcpy(body.data() + i, &net_root_delay, sizeof(net_root_delay));
        i = body.size();
        body.resize(i + sizeof(uint32_t));
        uint32_t net_root_dispersion = htobe32(unsigned_fixed32_from_double(root_dispersion));
        std::memcpy(body.data() + i, &net_root_dispersion, sizeof(net_root_dispersion));
        i = body.size();
        body.resize(i + 4);
        std::memcpy(body.data() + i, reference_clock_identifier, 4);
        i = body.size();
        body.resize(i + sizeof(uint64_t));
        uint64_t net_reference =  htobe64(ntp_timestamp_from_nano_since1970(reference_timestamp));
        std::memcpy(body.data() + i, &net_reference, sizeof(net_reference));
        i = body.size();
        body.resize(i + sizeof(uint64_t));
        uint64_t net_originate =  htobe64(ntp_timestamp_from_nano_since1970(originate_timestamp));
        std::memcpy(body.data() + i, &net_originate, sizeof(net_originate));
        i = body.size();
        body.resize(i + sizeof(uint64_t));
        uint64_t net_receive =  htobe64(ntp_timestamp_from_nano_since1970(receive_timestamp));
        std::memcpy(body.data() + i, &net_receive, sizeof(net_receive));
        i = body.size();
        body.resize(i + sizeof(uint64_t));
        uint64_t net_transmit = htobe64(ntp_timestamp_from_nano_since1970(transmit_timestamp));
        std::memcpy(body.data() + i, &net_transmit, sizeof(net_transmit));
        return body;
    }

    double double_from_unsigned_fixed32(uint32_t fixed) {
        return (double)fixed / (double)( 1ull << 16 );
    }

    uint32_t unsigned_fixed32_from_double(double input) {
        return (uint32_t) (input * (double)( 1ull << 16 ));
    }

    double double_from_signed_fixed32(int32_t fixed) {
        return (double)fixed / (double)( 1ll << 16 );
    }

    int32_t signed_fixed32_from_double(double input) {
        return (int32_t) (input * (double)( 1ll << 16 ));
    }

    std::chrono::nanoseconds nano_since1970_from_ntp_timestamp(uint64_t input) {
        uint64_t secs_since1900 = input >> 32;
        uint64_t frac_part = input - (secs_since1900 << 32);
        uint64_t frac_as_nanos = frac_part/(4294967296.0/1000000000.0);
        uint64_t secs_since1970 = secs_since1900 - 2208988800;
        uint64_t nanos_since1970 = secs_since1970*1000000000 + frac_as_nanos;
        return std::chrono::nanoseconds(nanos_since1970);
    }

    uint64_t ntp_timestamp_from_nano_since1970(std::chrono::nanoseconds input) {
        uint64_t nanos_since1970 = input.count();
        uint64_t secs_since1970 = nanos_since1970/1000000000;
        uint64_t frac_as_nanos = nanos_since1970 - (secs_since1970*1000000000);
        uint64_t frac_part = frac_as_nanos*(4294967296.0/1000000000.0);
        uint64_t secs_since1900 = secs_since1970 + 2208988800;
        uint64_t ntp_timestamp = (secs_since1900 << 32) + frac_part;
        return ntp_timestamp;
    }

    double NTPStats::offset() {
        return (double)(((mPacket.receive_timestamp - mPacket.originate_timestamp) + (mPacket.transmit_timestamp - mPacket.destination_timestamp)).count())/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::offset_nanoseconds() {
        return ((mPacket.receive_timestamp - mPacket.originate_timestamp) + (mPacket.transmit_timestamp - mPacket.destination_timestamp));
    }

    double NTPStats::delay() {
        return (double)(((mPacket.destination_timestamp - mPacket.originate_timestamp) - (mPacket.transmit_timestamp - mPacket.receive_timestamp)).count())/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::delay_nanoseconds() {
        return ((mPacket.destination_timestamp - mPacket.originate_timestamp) - (mPacket.transmit_timestamp - mPacket.receive_timestamp));
    }

    double NTPStats::tx_time() {
        return (double)(mPacket.transmit_timestamp).count()/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::tx_time_nanoseconds() {
        return mPacket.transmit_timestamp;
    }

    double NTPStats::recv_time() {
        return (double)(mPacket.receive_timestamp).count()/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::recv_time_nanoseconds() {
        return mPacket.receive_timestamp;
    }

    double NTPStats::orig_time() {
        return (double)(mPacket.originate_timestamp).count()/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::orig_time_nanoseconds() {
        return mPacket.originate_timestamp;
    }

    double NTPStats::ref_time() {
        return (double)(mPacket.reference_timestamp).count()/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::ref_time_nanoseconds() {
        return mPacket.reference_timestamp;
    }

    double NTPStats::dest_time() {
        return (double)(mPacket.destination_timestamp).count()/1000000000.0;
    }
    std::chrono::nanoseconds NTPStats::dest_time_nanoseconds() {
        return mPacket.destination_timestamp;
    }

    NTPStats NTPClient::request(std::string host, uint16_t port) {
        io_context ctx;
        udp::resolver resolver(ctx);
        udp::socket sock(ctx, udp::endpoint(udp::v4(), 0));
        udp::endpoint target_endpoint = *resolver.resolve(host, "ntp");
        auto now_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());

        Packet out_packet{
            0, // leap
            3, // version = NTPv3
            3, // mode = Client
            0, // stratum
            0, // poll
            0, // precision
            0, // root delay
            0, // root dispersion
            "\0\0\0\0", // reference id
            std::chrono::nanoseconds(0), // reference timestamp
            std::chrono::nanoseconds(0), // originate timestamp
            std::chrono::nanoseconds(0), // receive timestamp
            now_ns, // transmit timestamp
            std::chrono::nanoseconds(0) // destination timestamp
        };
        auto out_buff = out_packet.serialize();
        sock.send_to(buffer(out_buff, out_buff.size()), target_endpoint);
        std::chrono::nanoseconds time_expended = 0ns;
        udp::endpoint sender_endpoint;
        std::vector<uint8_t> reply;
        reply.resize(1500);
        while (time_expended < 1s)
        {
            auto start_time = std::chrono::system_clock::now();
            auto ready  = sock.available();
            if ( ready >= 48) {
                size_t reply_length = sock.receive_from(
                        buffer(reply.data(), 1500), sender_endpoint
                );
                if (sender_endpoint.address().to_string() == target_endpoint.address().to_string())
                {
                    reply.resize(reply_length);
                    break;
                }
            }
            auto end_time = std::chrono::system_clock::now();
            time_expended += end_time-start_time;
        }
        if (time_expended >= 1000ms){
            throw std::runtime_error("no response");
        }
        Packet response = Packet::deserialize(reply);
        now_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());
        response.destination_timestamp = now_ns;
        return NTPStats(response);
    }
}
